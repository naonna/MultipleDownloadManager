package com.example.okorchysta.multipledownloadmanager.downloadManager;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.example.okorchysta.multipledownloadmanager.DownloadManagerCallback;

import java.util.List;

public class MultipleDownloadManager implements DownloadListener {

    private static final String TAG = "MultipleDownloadManager";

    private DownloadManagerCallback view;
    private DownloadManagerCore d;

    public MultipleDownloadManager(DownloadManagerCallback view){
        this.view = view;
    }

    public boolean prepareDownloadManager(Activity activity){
        if (permissionGranted(activity)){
            prepare(activity);
            return true;
        } else {
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    DownloadManagerCore.PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
            return false;
        }
    }

    public void download(Context context, List<String> urlList) {
        try {
            d.downloadSetOfItems(context, urlList);
        } catch (NullPointerException e){
            Log.e(TAG, "CALL prepareDownloadManager(Activity activity) BEFORE DOWNLOADING");
        }
    }

    public void releaseDownloadManager(Context context){
        if (d!=null){
            d.releaseDownloadManager(context);
        }
    }

    private void prepare(Context context){
        d = DownloadManagerCore.newInstance(context, this);
    }

    private boolean permissionGranted(Context context){
        return (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED);
    }


    @Override
    public void onDownloadCompleted() {
      view.onDownloadCompleted();
    }
}
