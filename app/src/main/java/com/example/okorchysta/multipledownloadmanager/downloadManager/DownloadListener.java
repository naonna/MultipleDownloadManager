package com.example.okorchysta.multipledownloadmanager.downloadManager;

public interface DownloadListener {
    void onDownloadCompleted();
}
