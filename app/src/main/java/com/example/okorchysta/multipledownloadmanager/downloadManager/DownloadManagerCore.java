package com.example.okorchysta.multipledownloadmanager.downloadManager;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class DownloadManagerCore {
    public final static int PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1;
    private static final String TAG = "DownloadManagerCore";

    private DownloadManager downloadManager;
    private BroadcastReceiver onComplete;

    private List<Long> idList = new ArrayList<>();
    private DownloadListener downloadListener;

    private static DownloadManagerCore instance;

    static DownloadManagerCore newInstance(Context context, DownloadListener view) {
        if (instance!=null) return instance;
        instance = new DownloadManagerCore(context, view);
        return instance;
    }

    private DownloadManagerCore(Context context, DownloadListener view){
        downloadManager = (DownloadManager)context.getSystemService(Context.DOWNLOAD_SERVICE);
        this.downloadListener = view;
        createReceiver();
    }

    private void createReceiver(){
        onComplete = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
                Log.d(TAG, "onReceive: ");
                idList.remove(referenceId);
                if (idList.isEmpty()) {
                    if (downloadListener !=null) {
                        downloadListener.onDownloadCompleted();
                    }
                    releaseDownloadManager(context);
                }
            }
        };
    }

    void downloadSetOfItems(Context context, List<String> urlList){
        if (urlList.isEmpty()) return;
        context.getApplicationContext().registerReceiver(onComplete,
                new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

        for (String url: urlList) {
            Uri uri = Uri.parse(url);
            DownloadManager.Request request = new DownloadManager.Request(uri);
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
            request.setTitle("Title");
            request.setDescription("Description");
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "/multipleDownload/"  + "/" + "Sample_" + urlList.indexOf(url) + ".png");

            long id = downloadManager.enqueue(request);
            idList.add(id);
        }
    }

    void releaseDownloadManager(Context context){
        if (context!=null){
            try {
                context.getApplicationContext().unregisterReceiver(onComplete);
            } catch (IllegalArgumentException e){
                e.printStackTrace();
            }
        }
        idList.clear();
    }



}
