package com.example.okorchysta.multipledownloadmanager;

import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.okorchysta.multipledownloadmanager.downloadManager.DownloadManagerCore;
import com.example.okorchysta.multipledownloadmanager.downloadManager.MultipleDownloadManager;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, DownloadManagerCallback {

    List<String> list;

    MultipleDownloadManager dm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn = findViewById(R.id.btn);
        btn.setOnClickListener(this);

        list = new ArrayList<>();
        list.add("https://upload.wikimedia.org/wikipedia/commons/thumb/3/36/Hopetoun_falls.jpg/1200px-Hopetoun_falls.jpg");
        list.add("https://upload.wikimedia.org/wikipedia/commons/thumb/8/8f/Bachalpsee_reflection.jpg/1280px-Bachalpsee_reflection.jpg");
        list.add("https://upload.wikimedia.org/wikipedia/commons/5/57/Galunggung.jpg");
        list.add("https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/KERALA_-_32.jpg/1920px-KERALA_-_32.jpg");
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn){
            dm = new MultipleDownloadManager(this);
            boolean prepared = dm.prepareDownloadManager(this);
            if (prepared) dm.download(this, list);

        }
    }

    @Override
    public void onDownloadCompleted() {
        Toast.makeText(this, "DOWNLOADED", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dm!=null) dm.releaseDownloadManager(this);
    }


    void showError(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case DownloadManagerCore.PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    boolean prepared = dm.prepareDownloadManager(this);
                    if (prepared){
                        dm.download(this, list);
                    }

                } else {
                    showError("Need permissions");
                }

            }
        }
    }
}
