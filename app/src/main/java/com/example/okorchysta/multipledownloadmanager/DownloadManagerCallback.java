package com.example.okorchysta.multipledownloadmanager;

public interface DownloadManagerCallback {
    void onDownloadCompleted();
}
